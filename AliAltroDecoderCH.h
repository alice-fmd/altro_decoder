/**
 * @file   AliAltroDecoderCH.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Oct  8 02:55:37 2008
 * 
 * @brief  ALTRO decoder
 * 
 * 
 */
#ifndef ALIALTRODECODERCH_H
#define ALIALTRODECODERCH_H
#include <TObject.h>

/**
 * @mainpage main ALTRO decoder 
 *
 * @author Christian Holm Christensen <cholm@nbi.dk>
 *
 * This defines an ALTRO decoder. 
 *
 * The class drives the reading of the data.  Users can derive a class
 * from this class and reimplement the various @c Got... member
 * functions to process the data as it is read in. Compared to the
 * current @c AliAltroStreamer this has the advantage that both the
 * base code and the client code needs to do far less booking while
 * maintaining the full flexibility.
 *
 * An example of a derived class can be found in MyDecoder
 *
 * Here, the class @c MyDecoder simply writes out the data as it is
 * seen.  A more advanced (read: realistic) class would cache the RCU
 * ID, hardware address, and keep track of the time bins as needed.
 * See for example the class WrapperCH
 *
 * To use this class one can do something like
 * @include Test.C
 *
 * Defined is also the class AliAltroDecoderCHT which is the same as
 * AliAltroDecoderCH expect that it does not define a virtual
 * interface.  Instead, it relies on a template argument for the
 * decoding handling.  This is defined merely to show that there's
 * little overhead in virtual member function calls. 
 *
 * To factiltate testing, the class Wrapper is defined.  For more
 * information on this, refer to that class. 
 */
/** 
 * @defgroup decoders Decoders 
 */
/**
 * Class to decode ALTRO formated raw data.  
 * 
 * @ingroup decoders
 */
class AliAltroDecoderCH : public TObject 
{
public:
  /**
   * Error codes
   * 
   */
  enum { 
    /** Not enough number of bytes left in data for reading new word */
    kNotEnoughBytes = 1, 
    /** Didn't find an RCU trailer at the end of data */
    kInvalidRcuTrailer,
    /** Didn't find an expected channel trailer */
    kInvalidChannelTrailer, 
    /** Didn't find an expected fill word after channel trailer */
    kInvalidFillWord,
    /** Got a duplicate 40bit word */
    kDuplicate40BitWord, 
    /** Channel size from trailer, and distance to next channel does
	not match up */
    kChannelSizeMismatch,
    /** Saw an invalid bunch length */ 
    kInvalidBunchLength,
    /** Saw an invalid time */ 
    kInvalidTime
  };
  /** 
   * Constructor 
   * 
   */    
  AliAltroDecoderCH() 
    : TObject(), 
      fData(0), 
      fSize(0),
      fCheckChannelSize(kTRUE), 
      fCheckDuplicate40Bit(kTRUE), 
      fDecodeParameters(kTRUE),
      fCheckFillWords(kTRUE),
      fCursor(0) 
  {}

  /** 
   * Decode payload @a data of @a size bytes
   * 
   * @param data Payload data
   * @param size Size of payload in bytes
   * 
   * @return 0 on success, error code otherwise 
   */
  Int_t Decode(UChar_t* data, ULong_t size);
protected:
  /** @{
      @name Payload */
  /** Pointer to payload */
  UChar_t* fData;
  /** Size of payload */
  ULong_t  fSize;
  /** @} */
  /** @{ 
      @name Options */
  /** Whether to verify the channel size */
  Bool_t fCheckChannelSize;
  /** Whether to check for duplicate 40 bit words */
  Bool_t fCheckDuplicate40Bit;
  /** Whether to decode parameters */
  Bool_t fDecodeParameters;
  /** Whether to check fill words */ 
  Bool_t fCheckFillWords;
  /** @{ 
      @name Decoding of various parts */ 
  /** 
   * Decode the RCU trailer
   * 
   * 
   * @return 0 on success, error code otherwise 
   */
  Int_t DecodeRcuTrailer();
  /** 
   * Decode an ALTRO channel
   * 
   * 
   * @return 0 on success, error code otherwise 
   */
  Int_t DecodeChannel();
  /** 
   * Peek at 40bit words succeeding the current word to see if a
   * channel trailer can be found
   * 
   * 
   * @return 0 on success, negative error code otherwise 
   */
  Int_t ScanForNextChannel();
  /** 
   * Decode a bunch 
   * 
   * @param word  First 40bit word.  On return, the last read 40bit
   *        word. 
   * @param off   Current offset in passed 40bit word. On return, the
   *        current offset in the last read 40bit word. 
   * 
   * @return 0 on success, negative error code otherwise 
   */
  Int_t DecodeBunch(ULong64_t*& word, UShort_t& off);
  /** @} */


  /** @{ 
      @name Get data of different sizes */
  UShort_t Decode10Bits(UShort_t off, ULong64_t* w40, UShort_t& word) const;
  /** 
   * Extract a 32bit word from data starting at @a cur
   * 
   * @param cur 
   * 
   * @return 
   */  
  UInt_t Decode32Bits(UChar_t*& cur, UInt_t*& word) const;
  /** 
   * Extract a 40bit word from data starting at @a cur
   * 
   * @param cur 
   * 
   * @return 
   */
  ULong64_t Decode40Bits(UChar_t*& cur, ULong64_t*& word) const;
  /** 
   * Extract the 10bit word at offset @a off from the 40bit word @a
   * word.  If @a off points past the end of the 40bit word, a new
   * 40bit word is read in and returned in @a w40. 
   * 
   * @param w40  40bit word to extract from. 
   * @param w10  10bit word extracted.
   * @param off  10bit word offset
   * 
   * @return Zero or positive number on success, a negative error code
   *         on failure.  
   */
  Int_t  Get10Bits(UShort_t& off, ULong64_t*& w40, UShort_t& w10);
  /** 
   * Get a 32bit word from the data
   * 
   * @param word On return, the 32bit word. 
   * 
   * @return Positive number on success, a negative error code on failure. 
   */
  Int_t     Get32Bits(UInt_t*& word);
  /** 
   * Get a 40bit word from the data
   * 
   * @param word On return, the 40bit word. 
   * 
   * @return Positive number on success, a negative error code on failure. 
   */
  Int_t     Get40Bits(ULong64_t*& word);
  /** Pointer to current position in payload (from the back) */
  UChar_t* fCursor;
  /** @} */

  /** @{ 
      @name Peek data words */ 
  /** 
   * Peek at a 32bit at offset @a off from the current position.
   * 
   * @param off  Offset (in 32bit words)
   * @param word The 32bit at @a off from the current position
   * 
   * @return Positive number on success, a negative error code on failure. 
   */
  Int_t Peek32Bits(UShort_t off, UInt_t*& word) const;
  /** 
   * Peek at a 40bit at offset @a off from the current position.
   * 
   * @param off  Offset (in 40bit words)
   * @param word The 40bit at @a off from the current position
   * 
   * @return Positive number on success, a negative error code on failure. 
   */
  Int_t Peek40Bits(UShort_t off, ULong64_t*& word) const;
  /** 
   * Peek at a 10bit word at offset @a off from the current position
   * 
   * @param off  Offset (in 10bit words)
   * @param word 10bit word seen
   * 
   * @return Zero or positive number on success, a negative error code
   *         on failure.  
   */
  Int_t Peek10Bits(UShort_t off, UShort_t*& word) const;
  /** @} */

  /** @{ 
      @name Check on words */
  Bool_t IsRcuTrailer(UInt_t word) const;
  /** 
   * Check if a 40bit word matches a channel trailer. 
   * 
   * @param word Word to check
   * 
   * @return @c true if @a word matches a channel trailer. 
   */
  Bool_t IsChannelTrailer(ULong64_t word) const;
  /** 
   * Check if passed 10bit word @a word matches a fill word
   * 
   * @param word Word to check
   * 
   * @return @c true if the passed 10bit word matches a fill word
   */
  Bool_t IsFillWord(UShort_t word) const;
  /** Channel trailer mask */
  static const UInt_t fgkRcuTrailerMask;
  /** Channel trailer mask */
  static const ULong64_t fgkChannelTrailerMask;
  /** Fill word mask */
  static const UShort_t fgkFillMask;
  /** @} */

  /** @{ 
      @name Overwrite-able member functions. */
  /** 
   * Called when ever a new RCU (DDL) is seen.  The identifier is
   * passed in.   A user class can override this member function to
   * cache the current RCU identifier (for example). 
   * 
   * 
   * @return Should return 0 on success, negative error code otherwise 
   */
  virtual Int_t GotRcu(UShort_t /*id*/) { return 0; }
  /** 
   * Called whenever a parameter from the RCU trailer is seen.  The
   * parameter code is passed as @a id, and the value is passed in @a
   * val 
   * 
   * @param id Parameter code
   * @param val Parameter value
   * 
   * @return Should return 0 on success, negative error code otherwise 
   */
  virtual Int_t GotParameter(UShort_t id, UInt_t val) { return 0; }
  /** 
   * Called when we know the number of 40bit words in the payload. 
   * 
   * @param n40 Number of 40bit words in payload
   * 
   * @return Should return @a n40 on success, negative error code
   *         otherwise. 
   */
  virtual Int_t GotPayloadSize(ULong_t n40) { return n40; }
  /** 
   * Called whenever a channel trailer is seen.  The hardware address
   * @a hwaddr and number of 10bit words @a n10 is passed in. 
   * 
   * @param hwaddr 
   * @param n10 
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */
  virtual Int_t GotChannel(UShort_t hwaddr, UShort_t n10) { return 0; }
  /** 
   * Called whenever we get a new bunch
   * 
   * @param len  Total length of the bunch - including length and time word.
   * @param time Time of last ADC value in the bunch
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */  
  virtual Int_t GotBunch(UShort_t len, UShort_t time) { return 0; }
  /** 
   * Called whenever we read a new ADC count. 
   * 
   * @param time Time of the ADC count
   * @param adc  ADC count value
   * 
   * @return  Should return 0 on success, negative error code otherwise  
   */
  virtual Int_t GotTimebin(UShort_t time, UShort_t adc) { return 0; }
  /** 
   * Called whenever we get an invalid trailer.  Users can override
   * this to do specific error handling. 
   * - Return 0 or positive number: go on as best we can.
   * - Return negative error code: drop this RCU. 
   * 
   * @param trailer Failed trailer. 
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */
  virtual Int_t GotInvalidRcuTrailer(UInt_t trailer);
  /** 
   * Called whenever we get an invalid trailer.  Users can override
   * this to do specific error handling. 
   * - Return 0 or positive number: go on, and try to decode the
   *   channel anyway. 
   * - Return negative error code: Drop this channel and search for
   *   the next channel. 
   * 
   * @param trailer Failed trailer. 
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */
  virtual Int_t GotInvalidChannelTrailer(ULong64_t trailer);
  /** 
   * Called when-ever we get an invalid fill word.  Users can override
   * this to do specific things in these kinds of errors.  
   * - Return 0 or positive number: go on. 
   * - Return negative number: Dropt this channel.
   * 
   * @param fill Invalid fill word seen 
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */
  virtual Int_t GotInvalidFillWord(UShort_t fill);
  /** 
   * Called whenever we get a duplicate 40bit word. 
   * Let user decide what to do with this 
   * - The user can simply ignore this by returning 0.
   * - The user can read in a new 40bit word and return that in @a
   *   second and return 0.
   * - The user can return a negative error code.
   * 
   * @param first  First word
   * @param second Second word - which has the same value as @a first
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */
  virtual Int_t GotDuplicate40BitWord(ULong64_t first, ULong64_t& second) {
    return -kDuplicate40BitWord; }
  /** 
   * Called when ever we're skipping over 40bit words when searching
   * for the next channel. 
   * 
   * @param word Word skipped. 
   * 
   * @return Should return 0 on success, negative error code
   *         otherwise. 
   */
  virtual Int_t GotIgnoredWord(ULong64_t word) { return 0; }
  /** 
   * Called when ever the channel length does not add up to a full
   * channel.  Users can override this to do error recovery. 
   * - Return 0 on positive number: go on as best we can. 
   * - Return negative number: Drop this channel and search for a new
   *   one. 
   * 
   * @param len    Length of the channel in 10bit words
   * @param hwaddr Hardware address of channel
   * @param next   40bit word that should be the next trailer
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */  
  virtual Int_t GotChannelSizeMismatch(UShort_t len, UShort_t hwaddr, 
				       ULong64_t next) { 
    return -kChannelSizeMismatch; }
  /** 
   * Called when ever we see a bunch with a base time @a time that and
   * length @a len that would generate negative times. Users can
   * override this to do error handling. 
   * - Return 0 or positive number: keep going
   * - Return negative error code: Skip this bunch.
   * 
   * @param len  Length of bunch 
   * @param time Time of bunch. 
   * 
   * @return Should return 0 on success, negative error code. 
   */
  virtual Int_t GotInvalidTime(UShort_t len, UShort_t time) {
    return -kInvalidTime; }
  /** @} */


  /** @{
      @name Utility functions */
  /** 
   * Get the board number from the hardwar address
   * 
   * @param hwaddr Hardware address
   * 
   * @return The board numer
   */  
  static UShort_t Board(UShort_t hwaddr) { return (hwaddr >> 7) & 0x1F; }
  /** 
   * Get the altro number from the hardwar address
   * 
   * @param hwaddr Hardware address
   * 
   * @return The altro numer
   */  
  static UShort_t Altro(UShort_t hwaddr) { return (hwaddr >> 4) & 0x7; }
  /** 
   * Get the channel number from the hardwar address
   * 
   * @param hwaddr Hardware address
   * 
   * @return The channel numer
   */  
  static UShort_t Channel(UShort_t hwaddr) { return (hwaddr >> 0) & 0xF; }
  /** @} */

  ClassDef(AliAltroDecoderCH,0)
};

#ifndef __CINT__
//____________________________________________________________________
const UInt_t AliAltroDecoderCH::fgkRcuTrailerMask = 0xaaaa0000;
const ULong64_t AliAltroDecoderCH::fgkChannelTrailerMask = 
  ((ULong64_t(0x2aaa) << 26) | (ULong64_t(0xa) << 12));
const UShort_t AliAltroDecoderCH::fgkFillMask     = 0x2aa;

//____________________________________________________________________
Bool_t 
AliAltroDecoderCH::IsRcuTrailer(UInt_t word) const
{
  return ((word & fgkRcuTrailerMask) == fgkRcuTrailerMask);
}
//____________________________________________________________________
Bool_t 
AliAltroDecoderCH::IsChannelTrailer(ULong64_t word) const
{
  return ((word & fgkChannelTrailerMask) == fgkChannelTrailerMask);
}
//____________________________________________________________________
Bool_t 
AliAltroDecoderCH::IsFillWord(UShort_t word) const
{
  return ((word & fgkFillMask) == fgkFillMask);
}


//____________________________________________________________________
UShort_t
AliAltroDecoderCH::Decode10Bits(UShort_t off, ULong64_t* w40, 
				UShort_t& w10) const
{
  UShort_t roff = (4-1-off++);
  return w10 = ((*w40 >> (roff * 10)) & 0x3FF);
}

//____________________________________________________________________
UInt_t 
AliAltroDecoderCH::Decode32Bits(UChar_t*& cur, UInt_t*& word) const
{
  cur   -= 4;
  word =  reinterpret_cast<UInt_t*>(cur);
  return *word;
}

//____________________________________________________________________
ULong64_t
AliAltroDecoderCH::Decode40Bits(UChar_t*& cur, ULong64_t*& word) const
{
  cur   -= 5;
  word =  reinterpret_cast<ULong64_t*>(cur);
  
  return *word;
}  

//____________________________________________________________________
Int_t
AliAltroDecoderCH::Get10Bits(UShort_t& off, ULong64_t*& w40, UShort_t& w10)
{
  Int_t     ret = 0;
  if (off >= 4) { 
    ULong64_t old = *w40;
    if ((ret = Get40Bits(w40)) < 0) return ret;
    if (fCheckDuplicate40Bit && old == *w40) 
      if ((ret =  GotDuplicate40BitWord(old,*w40)) < 0) return ret;
    off = 0;
  }
  Decode10Bits(off++, w40, w10);
  return ret;
}  

//____________________________________________________________________
Int_t 
AliAltroDecoderCH::Get32Bits(UInt_t*& word)
{
  if ((fCursor - fData) < 4) return -kNotEnoughBytes;

  Decode32Bits(fCursor, word);
  return 4;
}

//____________________________________________________________________
Int_t
AliAltroDecoderCH::Get40Bits(ULong64_t*& word)
{
  if ((fCursor - fData) < 5) return -kNotEnoughBytes;

  Decode40Bits(fCursor, word);
  return 5;
}  


//____________________________________________________________________
Int_t 
AliAltroDecoderCH::Peek10Bits(UShort_t off, UShort_t*& word) const
{
  UShort_t   off40 = off / 4;
  UShort_t   off10 = off % 4;
  ULong64_t* w40   = 0;
  Int_t      ret = 0;
  if ((ret = Peek40Bits(off40, w40)) < 0) return ret;
  Decode10Bits(off10, w40, *word);
  return 0;
}

//____________________________________________________________________
Int_t 
AliAltroDecoderCH::Peek32Bits(UShort_t off, UInt_t*& word) const
{
  UChar_t* base = fCursor - (off-1) * 4;
  if ((base - fData) < 4) return -kNotEnoughBytes;

  Decode32Bits(base, word);
  return 4;
}

//____________________________________________________________________
Int_t
AliAltroDecoderCH::Peek40Bits(UShort_t off, ULong64_t*& word) const
{
  UChar_t* base = fCursor - (off-1) * 5;
  if ((base - fData) < 5) return -kNotEnoughBytes;

  Decode40Bits(base, word);
  return 5;
}  

//____________________________________________________________________
Int_t 
AliAltroDecoderCH::Decode(UChar_t* data, ULong_t size)
{
  fData    = data;
  fSize    = size;
  fCursor  = &(data[fSize]);
  Int_t     ret = 0;

  if ((ret = DecodeRcuTrailer()) < 0) return ret;
  
  ULong_t  n40    = ret;
  ULong_t  i40    = 0;
  UShort_t npad = ((fCursor-fData) % 5);
  fCursor -= npad;
  
  while (i40 < n40) { 
    ret = DecodeChannel();
    if (ret == -kNotEnoughBytes) return 0;

    if (ret >= 0) { i40 += ret; continue; }
      
    if      (ret == -kInvalidChannelTrailer) ret = ScanForNextChannel();
    else if (ret == -kChannelSizeMismatch)   ret = ScanForNextChannel();
    else if (ret == -kDuplicate40BitWord)    ret = ScanForNextChannel();
    if (ret == -kNotEnoughBytes) return 0;
    if (ret < 0) return ret;
    
    i40 += ret;
  }
  

  return ret;
}

//____________________________________________________________________
Int_t 
AliAltroDecoderCH::DecodeRcuTrailer()
{
  UInt_t* word = 0;
  Int_t   ret  = 0;
  UInt_t  n40  = 0;
  if ((ret = Get32Bits(word)) < 0) return ret;

  if (!IsRcuTrailer(*word) && 
      (ret = GotInvalidRcuTrailer(*word)) < 0) return ret;
  
  UShort_t rcuId     = (*word >> 7) & 0x1FF;
  UShort_t nwTrailer = (*word >> 0) & 0x07F;
    
  if ((ret = GotRcu(rcuId)) < 0) return ret;

  if (fDecodeParameters) {
    for (Int_t i = 0; i < nwTrailer-2; i++) { 
      if ((ret = Get32Bits(word)) < 0) return ret;
      
      UShort_t param = (*word >> 26) & 0x7F;
      UInt_t   value = (*word >>  0) & 0x1FFFFFF;
      if ((ret = GotParameter(param, value)) < 0) return ret;
    }
  }
  else { 
    fCursor -= (nwTrailer-2) * sizeof(UInt_t);
  }
  
  if ((ret = Get32Bits(word)) < 0) return ret;
  n40 = *word;
  if ((ret = GotPayloadSize(n40)) < 0) return ret;
  ret = n40;
  return ret;
}

//____________________________________________________________________
Int_t 
AliAltroDecoderCH::DecodeChannel()
{
  Int_t      ret   = 0;
  ULong64_t* word  = 0;
  UChar_t*   start = fCursor;
  if ((ret = Get40Bits(word)) < 0) return ret;
  if (!IsChannelTrailer(*word) &&  
      (ret = GotInvalidChannelTrailer(*word)) < 0) return ret;
  
  UShort_t hwaddr  = (*word >> 0)  & 0xFFF;
  UShort_t last    = (*word >> 16) & 0x3ff;
  
  if ((ret = GotChannel(hwaddr, last)) < 0) return ret;
  if (last <= 0) return (start - fCursor) / 5;
  
  UShort_t   nfill   = (last % 4 == 0 ? 0 : 4 - last % 4);
  UShort_t   n40     = (last + nfill) / 4+1;
  ULong64_t* next    = 0;

  if (fCheckChannelSize && (ret = Peek40Bits(n40, next)) > 0) { 
    if (!IsChannelTrailer(*next) && 
	(ret = GotChannelSizeMismatch(last, hwaddr, *next)) < 0) 
      return ret;
  }

  if ((ret = Get40Bits(word)) < 0) return ret;

  if (fCheckFillWords) { 
    for (UShort_t i = 0; i < nfill; i++) { 
      UShort_t fill = 0;
      if ((ret = Get10Bits(i, word, fill)) < 0) return ret;
      if (!IsFillWord(fill) && (ret = GotInvalidFillWord(fill)) < 0) 
	return ret;
    }
  }
  
  Int_t    n10 = 0;
  UShort_t off = nfill;
  while (n10 < last) { 
    if ((ret = DecodeBunch(word, off)) < 0) return ret;
    n10 += ret;
  }
  
  ret = (start - fCursor) / 5;
  return ret;
}

//____________________________________________________________________
Int_t 
AliAltroDecoderCH::GotInvalidRcuTrailer(UInt_t trailer)
{
  return -kInvalidRcuTrailer;
}


//____________________________________________________________________
Int_t 
AliAltroDecoderCH::GotInvalidChannelTrailer(ULong64_t trailer)
{
  return -kInvalidChannelTrailer;
}

//____________________________________________________________________
Int_t 
AliAltroDecoderCH::GotInvalidFillWord(UShort_t fill)
{
  return -kInvalidFillWord;
}
      
//____________________________________________________________________
Int_t 
AliAltroDecoderCH::ScanForNextChannel()
{
  Int_t ret = 0;
  Int_t n40 = 0;
  
  do {
    ULong64_t* word = 0, *word1 = 0;
    if ((ret = Peek40Bits(1, word)) < 0) return ret;
    if (IsChannelTrailer(*word)) break;
    if ((ret = Get40Bits(word1)) < 0) return ret;
    n40++;
    // if ((ret = GotIgnoredWord(*word1)) < 0) return ret;
  } while (ret >= 0);
  ret = n40;
  return ret;
}

//____________________________________________________________________
Int_t 
AliAltroDecoderCH::DecodeBunch(ULong64_t*& word, UShort_t& off)
{
  UShort_t  len  = 0;
  UShort_t  time = 0;
  Int_t     ret  = 0;
  Int_t     n10  = 0;
  if ((ret = Get10Bits(off, word, len))  < 0) return ret;
  n10++;
  if ((ret = Get10Bits(off, word, time)) < 0) return ret;
  n10++;
  
  if ((ret = GotBunch(len, time)) < 0) return ret;
  Int_t endTime = (time-len+2);
  if (endTime < 0) {
    if ((ret = GotInvalidTime(len, time)) < 0) {
      UShort_t dummy = 0;
      for (UShort_t i = 0; i < len; i++) 
	if ((ret = Get10Bits(off, word, dummy)) < 0) return ret;
      return len;
    }
  }
  
  len -= 2;
  while (len > 0) { 
    UShort_t adc = 0;
    if ((ret = Get10Bits(off, word, adc)) < 0) return ret;
    n10++;
    len--;
    
    if ((ret = GotTimebin(time--, adc)) < 0) return ret;
  }
  return n10;
}
#endif
#endif
//____________________________________________________________________
// Local Variables:
//  mode: C++
// End:
