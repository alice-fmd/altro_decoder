/**
 * @file   WrapperCH.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Sun Nov 16 23:48:39 2008
 * 
 * @brief  Wrapper of decoder for uniform handling and output
 * 
 * 
 */
#ifndef __CINT__
# define __CINT__
# include <AliAltroDecoderCH.h>
# undef __CINT__
#else
# include <AliAltroDecoderCH.h>
#endif
#include <Wrapper.h>
#include <Channel.h>

/** 
 * Specific decoder implementation
 * 
 * 
 * @ingroup wrappers
 */
class Decoder : public AliAltroDecoderCH
{
public:
  Decoder() : fArray(0), fRcu(0), fCurrent(0) 
  {
    fCheckDuplicate40Bit = kFALSE;
    fCheckChannelSize    = kFALSE;
    fDecodeParameters    = kFALSE;
    fCheckFillWords	 = kFALSE;
  }
  void SetArray(TClonesArray* array) { fArray = array; }
  virtual Int_t GotRcu(UShort_t id) { fRcu = id; return 0; }
  virtual Int_t GotChannel(UShort_t hwaddr, UShort_t n10) 
  { 
    if (!fArray) return 0;
    Int_t n  = fArray->GetEntriesFast();
    fCurrent = new((*fArray)[n]) ::Channel(fRcu, 
					   this->Board(hwaddr), 
					   this->Altro(hwaddr), 
					   this->Channel(hwaddr));
    return 0; 
  }
  virtual Int_t GotTimebin(UShort_t time, UShort_t adc) 
  { 
    if (fCurrent) fCurrent->SetSignal(time, adc);
    return 0; 
  }
  Int_t GotInvalidFillWord(UShort_t) { return 0; }
  // Int_t GotDuplicate40BitWord(ULong64_t, ULong64_t& w40) { return 0; }
  void Reset() { fRcu = 0; fCurrent = 0; }
protected:     
  TClonesArray* fArray;
  UShort_t      fRcu; 
  ::Channel*    fCurrent;
};


/**
 * Wrapper of proposed decoder
 * 
 * @ingroup wrappers
 */
class WrapperCH : public Wrapper 
{
public:
  /** 
   * Constructor 
   * 
   * 
   */
  WrapperCH() 
    : Wrapper("chWrapper", "CH"), 
      fDecoder() 
  {}
  /** 
   * Decode one event.
   * 
   * @param array Cache to fill
   * @param data  Data to read
   * @param size  Size of @a data
   * 
   * @return @a true on success, @c false otherwise 
   */
  Bool_t DecodeEvent(TClonesArray* array, UChar_t* data, ULong_t size) 
  {
    fDecoder.SetArray(array);
    Int_t ret = fDecoder.Decode(data, size);
#if 1
    if (ret < 0) {
      std::string s("Unknown error");
      switch (-ret) { 
      case AliAltroDecoderCH::kNotEnoughBytes:
	s = "No more data"; 			break;
      case AliAltroDecoderCH::kInvalidRcuTrailer:	   
	s = "Missing RCU trailer";		break;
      case AliAltroDecoderCH::kInvalidChannelTrailer: 
	s = "Missing channel trailer";		break;
      case AliAltroDecoderCH::kInvalidFillWord:	   
	s = "Missing fill word";		break;
      case AliAltroDecoderCH::kDuplicate40BitWord:	   
	s = "Got a duplicate 40bit word";	break;
      case AliAltroDecoderCH::kChannelSizeMismatch:   
	s = "Channel size mismatch"; 		break;
      case AliAltroDecoderCH::kInvalidBunchLength:	   
	s = "Saw an invalid bunch length";	break;
      case AliAltroDecoderCH::kInvalidTime:	   
	s = "Saw an invalid time";		break;
      }
      std::cerr << "(Return: " << ret << "): " << s << std::endl;
    }
#endif
    fDecoder.Reset();
    return ret >= 0;
  }
  /** Decoder */
  Decoder fDecoder;

  ClassDef(WrapperCH,0)
};

  
// Local Variables:
//  mode: C++
// End:


