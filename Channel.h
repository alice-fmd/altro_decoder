// -*- mode: c++ -*- 
/**
 * @file   Channel.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Sun Nov 16 23:45:53 2008
 * 
 * @brief  Storage of channel data
 * 
 * 
 */
#ifndef CHANNEL_H
#define CHANNEL_H
#include <TObject.h>
#include <iostream>
#include <iomanip>

/**
 * Storage of channel data
 *
 * @ingroup wrappers
 * 
 */
struct Channel : public TObject 
{
  /** 
   * Constrictor 
   * 
   * @param rcu     RCU #
   * @param board   Board # 
   * @param altro   ALTRO #
   * @param channel Channel #
   */  
  Channel(UShort_t rcu=0,   UShort_t board=0, 
	  UShort_t altro=0, UShort_t channel=0)
    : fRcu(rcu), fBoard(board), fAltro(altro), fChannel(channel)
  {
    Clear();
  }
  /** 
   * Clear this.
   * 
   */  
  void Clear() { for (size_t i = 0; i < 1024; i++) fData[i] = 0;  }
  /** 
   * Set RCU #
   * 
   * @param rcu RCU number 
   */
  void SetRcu(UShort_t rcu)                 { fRcu     = rcu;     }
  /** 
   * Set board #
   * 
   * @param board Board number 
   */
  void SetBoard(UShort_t board)             { fBoard   = board;   }
  /** 
   * Set ALTRO #
   * 
   * @param altro ALTRO number 
   */
  void SetAltro(UShort_t altro)             { fAltro   = altro;   }
  /** 
   * Set channel #
   * 
   * @param channel Channel number 
   */
  void SetChannel(UShort_t channel)         { fChannel = channel; } 
  /** 
   * Set signal 
   * 
   * @param t   Timebin
   * @param adc Signal
   */
  void SetSignal(UShort_t t, UShort_t adc)  { fData[t] = adc; 	  }
  /** 
   * Get the RCU #
   * 
   *  
   * @return RCU #
   */
  UShort_t GetRcu()     const               { return fRcu;        }
  /** 
   * Get the board #
   * 
   *  
   * @return Board #
   */
  UShort_t GetBoard()   const               { return fBoard;      }
  /** 
   * Get the ALTRO #
   * 
   *  
   * @return ALTRO #
   */
  UShort_t GetAltro()   const               { return fAltro;      }
  /** 
   * Get the channel #
   * 
   *  
   * @return Channel #
   */
  UShort_t GetChannel() const               { return fChannel;    }
  /** 
   * Get signal at timebin @a t
   * 
   * @param t Timebin to look up
   * 
   * @return ADC value at @a t
   */
  UShort_t GetSignal(UShort_t t) const      { return fData[t];    }
protected:
  UShort_t fRcu;	// Rcu #
  UShort_t fBoard;	// Board #
  UShort_t fAltro;	// Altro #
  UShort_t fChannel;	// Channel #
  UShort_t fData[1024];	// Data 
  
  ClassDef(Channel, 1);
};

/** 
 * Print out a channel object
 * 
 * @param o Output stream
 * @param c Channel object
 * 
 * @return @a o 
 * @ingroup wrappers
 */
inline std::ostream&
operator<<(std::ostream& o, const Channel& c)
{
  return o << std::setfill('0') << std::setw(3) << c.GetRcu() << '/' << std::hex
	   << "0x" << std::setw(2) << c.GetBoard()   << '/' 
	   << "0x" << std::setw(1) << c.GetAltro()   << '/'
	   << "0x" << std::setw(1) << c.GetChannel() 
	   << std::dec << std::setfill(' ');
}


#endif
//
// EOF
//
