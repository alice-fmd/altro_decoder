/**
 * @file   MyDecoderT.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Sun Nov 16 23:46:54 2008
 * 
 * @brief  Example of a decoder that prints to standard out. 
 * 
 * 
 */
#ifndef MYDECODERT_H
#define MYDECODERT_H
#include <AliAltroDecoderCHT.h>
#include <iostream>
#include <iomanip>

/**
 * Handler for class template
 * 
 * @ingroup decoders
 */
struct MyHandler
{
  /** 
   * @{ 
   * @name Options 
  /** 
   * Whether to decode parameters 
   */
  Bool_t DecodeParameters() const { return kFALSE; }
  /** 
   * Whether to verify the channel size 
   */
  Bool_t CheckChannelSize() const { return kFALSE; }
  /** 
   * Whether to check for duplicate 40 bit words 
   */
  Bool_t CheckDuplicate40Bit() const { return kFALSE; }
  /** 
   * Whether to check fill words 
   */ 
  Bool_t CheckFillWords() const { return kFALSE; }
  /** @} */

  /** 
   * @{ 
   * @name Process and error member functions. 
   */
  /** 
   * Called when ever a new RCU (DDL) is seen.  The identifier is
   * passed in.   A user class can override this member function to
   * cache the current RCU identifier (for example). 
   * 
   * 
   * @return Should return 0 on success, negative error code otherwise 
   */
  Int_t GotRcu(UShort_t id) 
  { 
    std::cout << "Got RCU " << id << std::endl;
    return 0; 
  }
  /** 
   * Called whenever a parameter from the RCU trailer is seen.  The
   * parameter code is passed as @a id, and the value is passed in @a
   * val 
   * 
   * @param id Parameter code
   * @param val Parameter value
   * 
   * @return Should return 0 on success, negative error code otherwise 
   */
  Int_t GotParameter(UShort_t id, UInt_t val) 
  { 
    std::cout << " Got parameter " << std::hex << std::setfill('0')
	      << "0x" << std::setw(2) << id << ": "
	      << "0x" << std::setw(7) << val 
	      << std::dec << std::setfill(' ') << std::endl;
    return 0; 
  }
  /** 
   * Called when we know the number of 40bit words in the payload. 
   * 
   * @param n40 Number of 40bit words in payload
   * 
   * @return Should return @a n40 on success, negative error code
   *         otherwise. 
   */
  Int_t GotPayloadSize(ULong_t n40) 
  { 
    std::cout << " Got " << n40  << " 40bit words" << std::endl;
    return n40; 
  }
  /** 
   * Called whenever a channel trailer is seen.  The hardware address
   * @a hwaddr and number of 10bit words @a n10 is passed in. 
   * 
   * @param hwaddr 
   * @param n10 
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */
  Int_t GotChannel(UShort_t hwaddr, UShort_t n10) 
  { 
    std::cout << "  Got channel # : "
	      << "Board: " << std::setw(2) << Board(hwaddr)   << "  "
	      << "Chip: " << std::setw(1) << Altro(hwaddr)   << "  "
	      << "Channel: " << std::setw(2) << Channel(hwaddr) << "  "
	      << "Last: " << std::setw(4) << n10 << std::endl;
    return 0; 
  }
  /** 
   * Called whenever we get a new bunch
   * 
   * @param len  Total length of the bunch - including length and time word.
   * @param time Time of last ADC value in the bunch
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */  
  Int_t GotBunch(UShort_t len, UShort_t time) 
  { 
    std::cout << "   Got Bunch of size " << len << " ending at " << time 
	      << std::endl;
    return 0; 
  }
  /** 
   * Called whenever we read a new ADC count. 
   * 
   * @param time Time of the ADC count
   * @param adc  ADC count value
   * 
   * @return  Should return 0 on success, negative error code otherwise  
   */
  Int_t GotTimebin(UShort_t time, UShort_t adc) 
  { 
    std::cout << "    " << time << " -> 0x" << std::hex << adc 
	      << std::dec << std::endl;
    return 0; 
  }
  /** 
   * Called whenever we get an invalid trailer.  Users can override
   * this to do specific error handling. 
   * - Return 0 or positive number: go on as best we can.
   * - Return negative error code: drop this RCU. 
   * 
   * @param trailer Failed trailer. 
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */
  Int_t GotInvalidRcuTrailer(UInt_t trailer) 
  { 
    unsigned int mask = AliAltroDecoderCHT<MyHandler>::fgkRcuTrailerMask;
    std::cerr << "E: Invalid rcu trailer: " << std::hex << std::setfill('0') 
	      << "0x" << std::setw(8) << trailer 
	      << " (0x" << std::setw(8) << trailer 
	      << " & 0x" << std::setw(8) << mask
	      << " = 0x" << std::setw(8) << (trailer & mask) 
	      << ")" << std::dec << std::setfill(' ') << std::endl;
    return -kInvalidRcuTrailer; 
  }
  /** 
   * Called whenever we get an invalid trailer.  Users can override
   * this to do specific error handling. 
   * - Return 0 or positive number: go on, and try to decode the
   *   channel anyway. 
   * - Return negative error code: Drop this channel and search for
   *   the next channel. 
   * 
   * @param trailer Failed trailer. 
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */
  Int_t GotInvalidChannelTrailer(ULong64_t trailer)  
  { 
    ULong64_t mask = AliAltroDecoderCHT<MyHandler>::fgkChannelTrailerMask;
    std::cerr << "E: Invalid channel trailer: " << std::hex 
	      << std::setfill('0') 
	      << "0x" << std::setw(10) << trailer 
	      << " (0x" << std::setw(10) << trailer 
	      << " & 0x" << std::setw(10) <<  mask
	      << " = 0x" << std::setw(10) << (trailer & mask) << ")" 
	      << std::dec << std::setfill(' ') << std::endl;
    return -kInvalidChannelTrailer;
  }
  /** 
   * Called when-ever we get an invalid fill word.  Users can override
   * this to do specific things in these kinds of errors.  
   * - Return 0 or positive number: go on. 
   * - Return negative number: Dropt this channel.
   * 
   * @param fill Invalid fill word seen 
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */
  Int_t GotInvalidFillWord(UShort_t fill) 
  { 
    std::cerr << "W: Invalid fill word: " << std::hex << std::setfill('0') 
	      << "0x" << std::setw(2) << fill 
	      << std::dec << std::setfill(' ') << std::endl;
    return -kInvalidFillWord; 
  }
  /** 
   * Called whenever we get a duplicate 40bit word. 
   * Let user decide what to do with this 
   * - The user can simply ignore this by returning 0.
   * - The user can read in a new 40bit word and return that in @a
   *   second and return 0.
   * - The user can return a negative error code.
   * 
   * @param first  First word
   * @param second Second word - which has the same value as @a first
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */
  Int_t GotDuplicate40BitWord(ULong64_t first, ULong64_t& second) 
  {
    std::cerr << "W: Got duplicate: " << std::hex << std::setfill('0')  
	      << "0x" << std::setw(10) << first 
	      << " == 0x" << std::setw(10) << second 
	      << std::dec << std::setfill(' ') << std::endl;
    return -kDuplicate40BitWord;
  }
  /** 
   * Called when ever we're skipping over 40bit words when searching
   * for the next channel. 
   * 
   * @param word Word skipped. 
   * 
   * @return Should return 0 on success, negative error code
   *         otherwise. 
   */
  Int_t GotIgnoredWord(ULong64_t word) 
  { 
    std::cout << "W: Ignoring " 
	      << std::hex << std::setfill('0') 
	      << std::setw(10) << word 
	      << std::dec << std::setfill(' ') << std::endl;
    return 0; 
  }
  /** 
   * Called when ever the channel length does not add up to a full
   * channel.  Users can override this to do error recovery. 
   * - Return 0 on positive number: go on as best we can. 
   * - Return negative number: Drop this channel and search for a new
   *   one. 
   * 
   * @param len    Length of the channel in 10bit words
   * @param hwaddr Hardware address of channel
   * @param next   40bit word that should be the next trailer
   * 
   * @return Should return 0 on success, negative error code otherwise  
   */  
  Int_t GotChannelSizeMismatch(UShort_t len, UShort_t hwaddr, ULong64_t next) 
  { 
    std::cout << "W: Length " << len << " of channel 0x" 
	      << std::hex << std::setfill('0') << std::setw(3) << hwaddr  
	      << " does not correspond time to 0x" 
	      << std::setw(10) << next << std::dec << std::setfill(' ') 
	      << " being a channel trailer word!" << std::endl;
    return -kChannelSizeMismatch; 
  }
  /** 
   * Called when ever we see a bunch with a base time @a time that and
   * length @a len that would generate negative times. Users can
   * override this to do error handling. 
   * - Return 0 or positive number: keep going
   * - Return negative error code: Skip this bunch.
   * 
   * @param len  Length of bunch 
   * @param time Time of bunch. 
   * 
   * @return Should return 0 on success, negative error code. 
   */
  Int_t GotInvalidTime(UShort_t len, UShort_t time) 
  { 
    std::cout << "W: Got invalid time " << time << " in bunch of length "
	      << len << std::endl;
    return -kInvalidTime; 
  }
  /** @} */

  /** @{
      @name Utility functions */
  /** 
   * Get the board number from the hardwar address
   * 
   * @param hwaddr Hardware address
   * 
   * @return The board numer
   */  
  static UShort_t Board(UShort_t hwaddr) { return (hwaddr >> 7) & 0x1F; }
  /** 
   * Get the altro number from the hardwar address
   * 
   * @param hwaddr Hardware address
   * 
   * @return The altro numer
   */  
  static UShort_t Altro(UShort_t hwaddr) { return (hwaddr >> 4) & 0x7; }
  /** 
   * Get the channel number from the hardwar address
   * 
   * @param hwaddr Hardware address
   * 
   * @return The channel numer
   */  
  static UShort_t Channel(UShort_t hwaddr) { return (hwaddr >> 0) & 0xF; }
  /** @} */
};

/**
 * Type of decoder
 *
 * @ingroup decoders
 */
typedef AliAltroDecoderCHT<MyHandler> MyDecoderT;

#endif
// Local Variables:
//  mode: C++
// End:

