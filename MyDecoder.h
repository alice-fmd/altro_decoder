/**
 * @file   MyDecoder.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Sun Nov 16 23:46:28 2008
 * 
 * @brief  Example of a decoder that prints to standard out. 
 * 
 * 
 */
#include <AliAltroDecoderCH.h>
#include <iostream>
#include <iomanip>

/**
 * Decoder class that simply writes out the data as it is seen. 
 *
 * Example of use 
 * @include Test.C
 * 
 * @ingroup decoders
 */
class MyDecoder : public AliAltroDecoderCH
{
  /** 
   * Print the current RCU id
   * 
   * @param id RCU identifier (as set in trailer)
   * 
   * @return 0. 
   */
  Int_t GotRcu(UShort_t id) 
  { 
    std::cout << "Got RCU " << id << std::endl;
    return 0; 
  }
  /** 
   * Print out a parameter value as set in trailer.
   * 
   * @param id  Parameter identifier
   * @param val Value 
   * 
   * @return 0
   */
  Int_t GotParameter(UShort_t id, UInt_t val) 
  {
    std::cout << " Got parameter " << std::hex << std::setfill('0')
	      << "0x" << std::setw(2) << id << ": "
	      << "0x" << std::setw(7) << val 
	      << std::dec << std::setfill(' ') << std::endl;
    return 0;
  }
  /** 
   * Print out how many 40bit words are in the payload
   * 
   * @param n40 Number of 40bit words. 
   * 
   * @return @a n40
   */  
  Int_t GotPayloadSize(ULong_t n40) 
  {
    std::cout << " Got " << n40  << " 40bit words" << std::endl;
    return n40;
  }
  /** 
   * Print out the channel address
   * 
   * @param hwaddr The hardware address.
   * @param n10    Number of 10bit words in channel data
   * 
   * @return 0
   */
  Int_t GotChannel(UShort_t hwaddr, UShort_t n10) 
  { 
    std::cout << "  Got channel # " << ((void*)(fCursor+5))  << ": "
	      << "Board: " << std::setw(2) << Board(hwaddr)   << "  "
	      << "Chip: " << std::setw(1) << Altro(hwaddr)   << "  "
	      << "Channel: " << std::setw(2) << Channel(hwaddr) << "  "
	      << "Last: " << std::setw(4) << n10 << std::endl;
    return 0; 
  }
  /** 
   * Print out a bunch length and last time.
   * 
   * @param len  Length of bunch
   * @param time Time index of bunch (time of last ADC value in bunch)
   * 
   * @return 0
   */
  Int_t GotBunch(UShort_t len, UShort_t time) 
  { 
    std::cout << "   Got Bunch of size " << len << " ending at " << time 
	      << std::endl;
    return 0; 
  }
  /** 
   * Print out the ADC value corresponding to a time-bin
   * 
   * @param time Time-bin index
   * @param adc  ADC value
   * 
   * @return 0
   */
  Int_t GotTimebin(UShort_t time, UShort_t adc) 
  { 
    std::cout << "    " << time << " -> 0x" << std::hex << adc 
	      << std::dec << std::endl;
    return 0; 
  }
  /** 
   * In case of an invalid RCU trailer, print out a message. 
   * 
   * @param trailer Wrong trailer seen. 
   * 
   * @return Error code
   */  
  Int_t GotInvalidRcuTrailer(UInt_t trailer) 
  { 
    Int_t ret = AliAltroDecoderCH::GotInvalidRcuTrailer(trailer);
    if (ret < 0) 
      std::cerr << "E: Invalid rcu trailer: " << std::hex << std::setfill('0') 
		<< "0x" << std::setw(8) << trailer 
	      << " (0x" << std::setw(8) << trailer 
		<< " & 0x" << std::setw(8) << fgkRcuTrailerMask 
		<< " = 0x" << std::setw(8) 
		<< (trailer & fgkRcuTrailerMask) << ")" 
		<< std::dec << std::setfill(' ') << std::endl;
    return ret;
  }
  /** 
   * In case of an invalid channel trailer, print out a message
   * 
   * @param trailer Wrong channel trailer
   * 
   * @return Error code 
   */
  Int_t GotInvalidChannelTrailer(ULong64_t trailer) 
  { 
    Int_t ret = AliAltroDecoderCH::GotInvalidChannelTrailer(trailer);
    if (ret < 0) 
      std::cerr << "E: Invalid channel trailer: " << std::hex 
		<< std::setfill('0') 
		<< "0x" << std::setw(10) << trailer 
		<< " (0x" << std::setw(10) << trailer 
		<< " & 0x" << std::setw(10) << fgkChannelTrailerMask 
		<< " = 0x" << std::setw(10) 
		<< (trailer & fgkChannelTrailerMask) << ")" 
		<< std::dec << std::setfill(' ') << std::endl;
    return ret;
  }
  /** 
   * In case of an invalid fill word, print out a message 
   * 
   * @param fill wrong fill word
   * 
   * @return Error code
   */
  Int_t GotInvalidFillWord(UShort_t fill) 
  { 
    Int_t ret = AliAltroDecoderCH::GotInvalidFillWord(fill);
    if (ret < 0)
      std::cerr << "W: Invalid fill word: " << std::hex << std::setfill('0') 
		<< "0x" << std::setw(2) << fill 
		<< std::dec << std::setfill(' ') << std::endl;
    return ret;
  }
  /** 
   * In case of a duplicate 40bit word, write out a message
   * 
   * @param first  First 40bit word
   * @param second Duplicate 40bit word of @a first
   * 
   * @return Error code
   */
  Int_t GotDuplicate40BitWord(ULong64_t  first, ULong64_t& second) 
  {
    std::cerr << "W: Got duplicate: " << std::hex << std::setfill('0')  
	      << "0x" << std::setw(10) << first 
	      << " == 0x" << std::setw(10) << second 
	      << std::dec << std::setfill(' ') << std::endl;
    return 0;
  }
  /** 
   * In case of an inconsistent channel size, write out a message
   * 
   * @param len    Expected length of channel data
   * @param hwaddr Hardware address of channel. 
   * @param next   The 40bit word expected to contain the next channel
   *        trailer. 
   * 
   * @return Error code
   */
  Int_t GotChannelSizeMismatch(UShort_t len, UShort_t hwaddr, ULong64_t next) 
  { 
    std::cerr << "E: Got channel size mismatch for " 
	      << std::hex << std::setfill('0') 
	      << "0x" << std::setw(2) << Board(hwaddr)   << "/"
	      << "0x" << std::setw(1) << Altro(hwaddr)   << "/"
	      << "0x" << std::setw(1) << Channel(hwaddr) 
	      << std::dec << std::setfill(' ') 
	      << " with supposedly " << len << " 10bit words - " 
	      << std::hex << std::setfill('0')  
	      << "0x" << std::setw(10) << next 
	      << std::dec << std::setfill(' ') 
	      << " does not match a trailer" << std::endl;
    return 0;
  }
  /** 
   * In case we're reading past 40bit words, write out a message
   * 
   * @param word Ignored 40bit word
   * 
   * @return 0
   */
  Int_t GotIgnoredWord(ULong64_t word)
  {
    std::cout << "W: Ignoring " 
	      << std::hex << std::setfill('0') 
	      << std::setw(10) << word 
	      << std::dec << std::setfill(' ') << std::endl;
    return 0;
  }
  /** 
   * In case of an invalid time (larger than last time, or length and
   * time of bunch does not match up), write a message
   * 
   * @param len  Length of bunch
   * @param time Time index of bunch.
   * 
   * @return Error code. 
   */
  Int_t GotInvalidTime(UShort_t len, UShort_t time)
  {
    std::cout << "W: Got invalid time " << time << " in bunch of length "
	      << len << std::endl;
    return -kInvalidTime;
  }
  
  ClassDef(MyDecoder,0)

};
// Local Variables:
//  mode: C++
// End:


