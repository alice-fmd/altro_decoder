/**
 * @file   WrapperCHT.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Sun Nov 16 23:49:03 2008
 * 
 * @brief  Wrapper of decoder for uniform handling and output
 * 
 * 
 */
#include <AliAltroDecoderCHT.h>
#include <Wrapper.h>
#include <Channel.h>

/** 
 * Handler 
 *
 * @ingroup wrappers 
 */
struct Handler
{
public:
  Handler() : fArray(0), fRcu(0), fCurrent(0) {}

  void SetArray(TClonesArray* array) { fArray = array; }
  void Reset() { fRcu = 0; fCurrent = 0; }

  Bool_t DecodeParameters() const { return kFALSE; }
  Bool_t CheckChannelSize() const { return kFALSE; }
  Bool_t CheckDuplicate40Bit() const { return kFALSE; }
  Bool_t CheckFillWords() const { return kFALSE; }

  Int_t GotRcu(UShort_t id) { fRcu = id; return 0; }
  Int_t GotParameter(UShort_t id, UInt_t val) { return 0; }
  Int_t GotPayloadSize(ULong_t n40) { return n40; }
  Int_t GotChannel(UShort_t hwaddr, UShort_t n10) 
  { 
    if (!fArray) return 0;
    Int_t n  = fArray->GetEntriesFast();
    fCurrent = new((*fArray)[n]) ::Channel(fRcu, 
					   this->Board(hwaddr), 
					   this->Altro(hwaddr), 
					   this->Channel(hwaddr));
    return 0; 
  }
  Int_t GotBunch(UShort_t len, UShort_t time) { return 0; }
  Int_t GotTimebin(UShort_t time, UShort_t adc) 
  { 
    if (fCurrent) fCurrent->SetSignal(time, adc);
    return 0; 
  }

  Int_t GotInvalidRcuTrailer(UInt_t trailer) { return -kInvalidRcuTrailer;  }
  Int_t GotInvalidChannelTrailer(ULong64_t trailer)
  {
    return -kInvalidChannelTrailer;
  }
  Int_t GotInvalidFillWord(UShort_t fill) { return -kInvalidFillWord; }
  Int_t GotDuplicate40BitWord(ULong64_t first, ULong64_t& second) 
  {
    return -kDuplicate40BitWord;
  }
  Int_t GotIgnoredWord(ULong64_t word) { return 0; }
  Int_t GotChannelSizeMismatch(UShort_t len, UShort_t hwaddr, ULong64_t next) 
  { 
    return -kChannelSizeMismatch; 
  }
  Int_t GotInvalidTime(UShort_t len, UShort_t time) { return -kInvalidTime; }

  static UShort_t Board(UShort_t hwaddr) { return (hwaddr >> 7) & 0x1F; }
  static UShort_t Altro(UShort_t hwaddr) { return (hwaddr >> 4) & 0x7; }
  static UShort_t Channel(UShort_t hwaddr) { return (hwaddr >> 0) & 0xF; }
protected:     
  TClonesArray* fArray;
  UShort_t      fRcu; 
  ::Channel*    fCurrent;
};

/** 
 * Type of decoder 
 *
 * @ingroup wrappers 
 */
typedef AliAltroDecoderCHT<Handler> DecoderT;

/**
 * Wrapper for class template 
 * 
 * @ingroup wrappers
 */
class WrapperCHT : public Wrapper 
{
public:
  /** 
   * Constructor 
   * 
   * 
   */
  WrapperCHT() 
    : Wrapper("chtWrapper", "CHT"), 
      fHandler(),
      fDecoder(fHandler) 
  {}
  /** 
   * Decode one event.
   * 
   * @param array Cache to fill
   * @param data  Data to read
   * @param size  Size of @a data
   * 
   * @return @a true on success, @c false otherwise 
   */
  Bool_t DecodeEvent(TClonesArray* array, UChar_t* data, ULong_t size) 
  {
    fHandler.SetArray(array);
    Int_t ret = fDecoder.Decode(data, size);
    if (ret < 0) {
      std::string s("Unknown error");
      switch (-ret) { 
      case kNotEnoughBytes:
	s = "No more data"; 			break;
      case kInvalidRcuTrailer:	   
	s = "Missing RCU trailer";		break;
      case kInvalidChannelTrailer: 
	s = "Missing channel trailer";		break;
      case kInvalidFillWord:	   
	s = "Missing fill word";		break;
      case kDuplicate40BitWord:	   
	s = "Got a duplicate 40bit word";	break;
      case kChannelSizeMismatch:   
	s = "Channel size mismatch"; 		break;
      case kInvalidBunchLength:	   
	s = "Saw an invalid bunch length";	break;
      case kInvalidTime:	   
	s = "Saw an invalid time";		break;
      }
      std::cerr << "(Return: " << ret << "): " << s << std::endl;
    }
    fHandler.Reset();
    return ret >= 0;
  }
  /** Handler */
  Handler  fHandler;
  /** Decoder */
  DecoderT fDecoder;
  
  ClassDef(WrapperCHT,0)
};

  
// Local Variables:
//  mode: C++
// End:


