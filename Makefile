#
#
#
# INPUT		:= /media/cholm_data/alice/data/08000053726000.10.root
INPUT		:= /media/cholm_data/alice/data/08000059807016.10.root

ROOT_LIBDIR    	:= $(shell root-config --libdir)
ROOT_INCDIR   	:= $(shell root-config --incdir)
ROOT_LIBS     	:= $(shell root-config --libs) -lVMC -lPhysics
ROOT_ALIBS    	:= -lRoot -ldl -lpcre -lfreetype -lz
ROOT_SOVERS   	:= $(dir $(shell root-config --version))
ROOT_RPATH    	:= -Wl,-rpath,$(ROOT_LIBDIR)/$(ROOT_SOVERS)
ALIROOT_INCDIR	:= $(ALICE_ROOT)/include
ALIROOT_LIBDIR	:= $(ALICE_ROOT)/lib/tgt_$(ALICE_TARGET)
ALIROOT_LIBS	:= -lRAWDatarec -lRAWDatabase -lESD -lSTEERBase 
ALIROOT_RPATH	:= -Wl,-rpath,$(ALIROOT_RPATH)

CPPFLAGS	:= -I$(ROOT_INCDIR) -I. 		\
		   -I$(ALIROOT_INCDIR) 			\
		   -I$(ALIROOT_INCDIR)/../RAW
CXX		:= g++ -c
CXXFLAGS	:= -g 
LD		:= g++ 
LDFLAGS		:= -L$(ROOT_LIBDIR)/$(ROOT_SOVERS) 	\
		   $(ROOT_RPATH) 			\
		   -L$(ALIROOT_LIBDIR)			\
		   $(ALIROOT_RPATH)			\
		   -Wl,-rpath,.
LIBS		:= $(ALIROOT_LIBS) $(ROOT_LIBS)
SO		:= g++ -shared
SOFLAGS		:= $(ALIROOT_RPATH) $(ROOT_RPATH)
REDIRECT	:= > $@.log 2>&1
ifneq ($(PROFILE), )
CXXFLAGS       += -pg
LDFLAGS	       += -pg -static 
LIBS	       := $(ALIROOT_LIBS) $(ROOT_ALIBS)
endif

SOURCES		= AliAltroDecoderCH.h 		\
		  Channel.h			\
		  Wrapper.h  			\
		  WrapperCH.h 			\
		  WrapperCHT.h 			\
		  WrapperStream.h		\
		  WrapperFast.h			\
		  MyDecoder.h 			\
		  MyDecoderT.h 			
SCRIPTS		= AliAltroDecoderCHT.h 		\
		  Run.C				\
		  DrawResult.C			\
		  Test.C			
EXTRA		= Doxyfile			\
		  Makefile			


%.o:%.cxx %.h
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< 

%Dict.h:%Dict.cxx
	@if test -f $@ ; then : ; else rm -f $< ; $(MAKE) $< ; fi

%Dict.cxx:%.h 
	rootcint -f $*Dict.cxx -c -I. $(CPPFLAGS) $< 

%.so:%Dict.o
	$(SO) $(SOFLAGS) -o $@ $^ 

hist%.root: run $(INPUT)
	./run $* $(INPUT)

%.prof.out:
	$(MAKE) clean 
	$(MAKE) PROFILE=1 
	./run $* 
	gprof -b ./run gmon.out

all:	$(SOURCES:%.h=%.so) run

all.root: run $(INPUT)
	./run All $(INPUT)

show:	all.root 
	root -l DrawResult.C

clean:
	rm -rf 			\
		*.o 		\
		*.so 		\
		*~ 		\
		*.d 		\
		*.tar.gz 	\
		*Dict.h 	\
		*Dict.cxx 	\
		*.log 		\
		html 		\
		latex		\
		run		

distclean: clean
	rm -f *.root gmon.out *.prof.out



run:	Run.o 			\
	AliAltroDecoderCHDict.o	\
	ChannelDict.o		\
	WrapperDict.o		\
	WrapperCHDict.o 	\
	WrapperCHTDict.o 	\
	WrapperFastDict.o 	\
	WrapperStreamDict.o
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS) 

test: 	all
	aliroot -l -q -b Test.C $(REDIRECT)

dist:
	mkdir altrodecoder 
	cp $(SOURCES) $(SCRIPTS) $(EXTRA) altrodecoder
	tar -czf altrodecoder.tar.gz altrodecoder
	rm -rf altrodecoder

doc:	Doxyfile $(SOURCES) $(SCRIPTS)
	doxygen $< 

WrapperCH.so:	AliAltroDecoderCH.so
WrapperCH2.so:	AliAltroDecoderCH2.so
MyDecoder.so:	AliAltroDecoderCH.so
MyDecoder2.so:	AliAltroDecoderCH2.so

#
# EOF
#
