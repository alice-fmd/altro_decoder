/**
 * @file   WrapperFast.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Sun Nov 16 23:49:11 2008
 * 
 * @brief  Wrapper of decoder for uniform handling and output
 * 
 * 
 */
#include <AliAltroDecoder.h>
#include <Wrapper.h>
#include <Channel.h>
#include <AliAltroData.h>
#include <AliAltroBunch.h>

/**
 * Wrapper around fast decoder 
 *
 * @ingroup wrappers
 */
class WrapperFast : public Wrapper 
{
public:
  /** 
   * Constructor
   * 
   */
  WrapperFast() 
    : Wrapper("fastWrapper", "Fast"), 
      fDecoder() 
  {}
  /** 
   * Decode one event.
   * 
   * @param array Cache to fill
   * @param data  Data to read
   * @param size  Size of @a data
   * 
   * @return @a true on success, @c false otherwise 
   */
  Bool_t DecodeEvent(TClonesArray* array, UChar_t* data, ULong_t size) 
  {
    data -= sizeof(AliRawDataHeader);
    size += sizeof(AliRawDataHeader);
    fDecoder.SetMemory(data, size);
    if (!fDecoder.Decode()) return kFALSE;
    
    AliAltroData adata;
    Bool_t ret = kTRUE;
    do { 
      ret = fDecoder.NextChannel(&adata);
      UChar_t*   x = 0;
      fDecoder.GetRCUTrailerData(x);
      UInt_t*    w = (UInt_t*)x;
      UShort_t   r = (w[9] >> 7) & 0x1FF;
      Int_t      n = array->GetEntriesFast();
      UInt_t     a = adata.GetHadd();
      ::Channel* c = new((*array)[n]) ::Channel(r, 
						this->Board(a), 
						this->Altro(a), 
						this->Channel(a));
      AliAltroBunch bunch;
      while (adata.NextBunch(&bunch)) { 
	const UInt_t* adcs = bunch.GetData();
	UInt_t        s    = bunch.GetStartTimeBin();
	UInt_t        e    = bunch.GetEndTimeBin();
	
	for (UShort_t t = s; t <= e; t++) 
	  c->SetSignal(t, adcs[t-s]);
      }
      adata.Reset();
    } while (ret);
#if 0
    std::cout << " Complete: " << fDecoder.GetComplete()
	      << " In-complete: " << fDecoder.GetIncomplete() 
	      << " Corrupt trailer: " 
	      << (fDecoder.IsFatalCorruptedTrailer() ? "yes" : "no")
	      << std::endl;
    std::cout << "Failure rate: " << fDecoder.GetFailureRate() << std::endl;
#endif
    return kTRUE;
  } 
  AliAltroDecoder fDecoder;

  ClassDef(WrapperFast,0)
};

  
// Local Variables:
//  mode: C++
// End:


