/**
 * @file   Wrapper.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Sun Nov 16 23:49:18 2008
 * 
 * @brief  Wrapper of decoder for uniform handling and output
 * 
 * 
 */
#ifndef WRAPPER_H
#define WRAPPER_H
#include <TTask.h>
#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TStopwatch.h>
#include <TH1.h>
#include <AliRawReader.h>
#include <iostream>
#include <Channel.h>

/** 
 * @defgroup wrappers Wrappers for testing 
 * 
 * This group defines a number of wrapper classes for testing the
 * various decoders against each other.  The idea is, that we have a
 * common interface, and a common output.  The execution is timed, and
 * we make sure that we compare the same thing in all cases.  That is,
 * we need to get to the same point in the decoding. 
 */
/**
 * base class for wrappers around decoders
 * 
 */
class Wrapper : public TTask
{
public:
  /** 
   * Destructor
   * 
   * 
   */
  virtual ~Wrapper() { if (fReader) delete fReader; }
  /** 
   * Make a raw reader
   * 
   * @param file Input. 
   * 
   * @return @c true on successs
   */
  virtual Bool_t MakeReader(const char* file)
  {
    if (!file || file[0] == '\0') { 
      std::cerr << "No file specified" << std::endl;
      return kFALSE;
    }

    fReader  = AliRawReader::Create(file);
    if (!fReader) { 
      std::cerr << "Failed to create reader" << std::endl;
      return kFALSE;
    }
    fReader->Reset();
    fReader->Select("FMD");
    return kTRUE;
  }
  /** 
   * Make output tree and file. 
   * 
   * 
   * @return @c true on success
   */  
  virtual Bool_t MakeTree()
  {
    TString outName(Form("%s.root", GetName()));
    fOut   = TFile::Open(outName.Data(), "RECREATE");
    if (!fOut) { 
      std::cerr << "Failed to make output file " << outName.Data() << std::endl;
      return kFALSE;
    }
    fTree  = new TTree("T", "T");
    fArray = new TClonesArray("Channel", 400);
    fTree->Branch("channel", fArray);

    fData     = new TH1F("data",     "ADC values", 1024, -.5, 1023.5);
    fRcu      = new TH1F("rcu",      "RCUs",        256, -.5,  255.5);
    fBoard    = new TH1F("board",    "Boards",       32, -.5,   31.5);
    fAltro    = new TH1F("altro",    "ALTROs",        8, -.5,    7.5);
    fChannel  = new TH1F("channel",  "Channels",     16, -.5,   15.5);
    fNChannel = new TH1F("nchannel", "# Channels", 1000, -.5,  999.5);

    return kTRUE;
  }
  /** 
   * Skip to first data
   * 
   * @param data On return, pointer to valid data
   * @param size On return, size of @a data in bytes
   * 
   * @return @c true on success, @c false on error or no more data.
   */
  virtual Bool_t SkipToData(UChar_t*& data, ULong_t& size)
  {
    do { 
      if (!fReader->ReadNextData(data)) return kFALSE;
    } while ((size = fReader->GetDataSize()) == 0);
    return kTRUE;
  }
  /** 
   * Execute the wrapper.  Loop over input data and fill output tree.
   * 
   * @param option No used
   */
  virtual void Exec(Option_t* option)
  {
    if (!MakeReader(option)) return;
    if (!MakeTree()) return;
    // TStopwatch fTimer;
    // if (fTimer.Counter()) fTimer.Reset();
    fNEvents = 0;
    fTimer.Reset();

    Int_t evno = -1;
    while ((evno = ReadEvent()) >= 0)  {
      if (evno <  20) continue;
      if (evno > 120) break;

      fArray->Clear();
      UChar_t* data  = 0;
      ULong_t  size  = 0;
      while (true) {
	if (!SkipToData(data, size)) break;

	
	fTimer.Start(kFALSE);
	Bool_t ret = DecodeEvent(fArray, data, size);
	fTimer.Stop();
	if (!ret) break;
	fNEvents++;
      }
      FillHistograms();

      fTree->Fill();
      fReader->Reset();
    }
    // std::cout << std::endl;
    fOut->Write();
    fOut->Close();
    // fTimer.Print();
    // delete tree;
    // delete array;
    // delete out;
    delete fReader;
    fReader  = 0;
  }
  /** 
   * Get the number of events analysed
   * 
   * 
   * @return 
   */
  Int_t GetNEvents() const { return fNEvents; }
  /** 
   * Fill histograms 
   * 
   */  
  void FillHistograms()
  {
    Int_t n = fArray->GetEntriesFast();
    // std::cout << "Got a total of " << n << " channels " << std::endl;
    fNChannel->Fill(n);
    for (Int_t i = 0; i < n; i++) { 
      ::Channel* c = static_cast< ::Channel* >(fArray->At(i));
      fRcu->Fill(c->GetRcu());
      fBoard->Fill(c->GetBoard());
      fAltro->Fill(c->GetAltro());
      fChannel->Fill(c->GetChannel());

      for (Int_t t = 0; t < 1024; t++) { 
	UShort_t adc = c->GetSignal(t);
	if (adc <= 0 || adc > 1023) continue;
	fData->Fill(adc);
      }
    }
  }
  
  /** 
   * Decode one event.  Should be implemented by derived class
   * 
   * @param array Array to fill with channel objects
   * @param data  Data to decode
   * @param size  Size of data 
   * 
   * @return @c true on success.
   */  
  virtual Bool_t DecodeEvent(TClonesArray* array, 
			     UChar_t* data, ULong_t size) = 0;
  
  /** 
   * Read one event.
   * 
   * 
   * @return Event number 
   */
  Int_t ReadEvent()
  {
    if (!fReader)              return -1;
    if (!fReader->NextEvent()) return -1;
    Int_t evno = fReader->GetEventIndex();
    // if (evno % 10 == 0) std::cout << "." << std::flush;
    // std::cout << "In event # " << evno << "/" 
    //           << fReader->GetNumberOfEvents() << std::endl;  
    return evno;
  }

  /** 
   * Get the board number from the hardwar address
   * 
   * @param hwaddr Hardware address
   * 
   * @return The board numer
   */  
  static UShort_t Board(UShort_t hwaddr) { return (hwaddr >> 7) & 0x1F; }
  /** 
   * Get the altro number from the hardwar address
   * 
   * @param hwaddr Hardware address
   * 
   * @return The altro numer
   */  
  static UShort_t Altro(UShort_t hwaddr) { return (hwaddr >> 4) & 0x7; }
  /** 
   * Get the channel number from the hardwar address
   * 
   * @param hwaddr Hardware address
   * 
   * @return The channel numer
   */  
  static UShort_t Channel(UShort_t hwaddr) { return (hwaddr >> 0) & 0xF; }

  TStopwatch    fTimer;
protected:
  /** 
   * Constructor 
   * 
   * @param name   Name 
   * @param title  Title
   * 
   * @return 
   */
  Wrapper(const char* name, const char* title) 
    : TTask(name, title), 
      // fTimer(),
      fReader(0) , 
      fOut(0),
      fTree(0),
      fArray(0)
  {}
  /** Raw reader */
  AliRawReader* fReader;
  /** output file */
  TFile*        fOut;
  /** Output tree */ 
  TTree*        fTree;
  /** Cache */
  TClonesArray* fArray;
  /** Histogram of data */
  TH1*          fData;
  /** Histogram of RCU numbers */
  TH1*          fRcu;
  /** Histogram of board numbers */
  TH1*          fBoard;
  /** Histogram of ALTRO numbers */
  TH1*          fAltro;
  /** Histogram of channel numbers */
  TH1*          fChannel;
  /** Histogram of Number channels read */
  TH1*          fNChannel;
  /** # of events analysed */
  Int_t         fNEvents;
  ClassDef(Wrapper,0)
};

#endif

  
// Local Variables:
//  mode: C++
// End:


