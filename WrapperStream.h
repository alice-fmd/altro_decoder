/**
 * @file   WrapperStream.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Sun Nov 16 23:49:48 2008
 * 
 * @brief   Wrapper of decoder for uniform handling and output
 * 
 * 
 */
#include <AliAltroRawStream.h>
#include <Wrapper.h>
#include <Channel.h>

/**
 * Wrapper around streamer decoder
 *
 * @ingroup wrappers
 */
class WrapperStream : public Wrapper 
{
public:
  /** 
   * Constructor 
   * 
   */
  WrapperStream() 
    : Wrapper("streamWrapper", "Stream"), 
      fStream(0), 
      fDone(kFALSE)
  {}
  /** 
   * Make a reader 
   * 
   * @param file Input
   * 
   * @return 
   */
  Bool_t MakeReader(const char* file)
  {
    Bool_t ret = kTRUE;
    if (!(ret = Wrapper::MakeReader(file))) return ret;
    fStream = new AliAltroRawStream(fReader);
    if (!fStream) ret = kFALSE;
    fStream->SetNoAltroMapping(kFALSE);
    return ret;
  }
  /** 
   * Skip to valid data
   * 
   * 
   * @return 
   */  
  Bool_t SkipToData(UChar_t*&, ULong_t&) 
  { 
    Bool_t ret = !fDone;
    fDone = ret;
    return ret; 
  }
  /** 
   * Decode one event.
   * 
   * @param array Cache to fill
   * @param data  Data to read
   * @param size  Size of @a data
   * 
   * @return @a true on success, @c false otherwise 
   */
  Bool_t DecodeEvent(TClonesArray* array, UChar_t*, ULong_t) 
  {
    Bool_t ret = kTRUE;
    do {
      ret = DecodeChannel(array);
    } while (ret);
    fDone = kTRUE;
    return kTRUE;
  }
  /** 
   * Decode a channel 
   * 
   * @param array Array to fill
   * 
   * @return @c true on success, @c false otherwise
   */
  Bool_t DecodeChannel(TClonesArray* array)
  {
    static Int_t last = 0xFFFF;
    Int_t        l    = 0;
    Bool_t       next = kTRUE;
    
    Int_t        n    = array->GetEntries();
    ::Channel*   c    = new ((*array)[n]) ::Channel();
    
    do { 
      Int_t signal = last;
      if (last > 0x3FF) { 
	next = fStream->Next();

	if (!next) { 
	  // No more data
	  if (fStream->GetPrevDDLNumber() < 0) { 
	    array->RemoveAt(n);
	    return kFALSE; // break;
	  }
	  
	  
	  UInt_t addr = fStream->GetPrevHWAddress();
	  c->SetRcu(fStream->GetPrevRCUId());
	  c->SetBoard(this->Board(addr));
 	  c->SetAltro(this->Altro(addr));
	  c->SetChannel(this->Channel(addr));
	  last = signal;
	  break;
	}
	
	signal = fStream->GetSignal();
	
	if ((fStream->GetHWAddress() != fStream->GetPrevHWAddress()) && 
	    (fStream->GetPrevHWAddress() >= 0)) { 
	  if (fStream->GetPrevDDLNumber() < 0) { 
	    array->RemoveAt(n);
	    return kFALSE; // break;
	  }
	  
	  UInt_t addr = fStream->GetPrevHWAddress();
	  c->SetRcu(fStream->GetPrevRCUId());
	  c->SetBoard(this->Board(addr));
 	  c->SetAltro(this->Altro(addr));
	  c->SetChannel(this->Channel(addr));
	  last = signal;
	  break;
	}
      }
      
      Int_t b = fStream->GetTimeLength();
      if (b < 1) { 
	last = 0xFFFF;
	continue;
      }
      

      Int_t t = fStream->GetTime();
      if (t < 0) { 
	last = 0xFFFF;
	continue;
      }

      l    = TMath::Max(l, t);
      c->SetSignal(t, signal);
      last = 0xFFFF;
    } while (next);
    return next;
  }
  /** Stream */
  AliAltroRawStream* fStream;
  /** Whether the event is done */
  Bool_t             fDone;
  
  ClassDef(WrapperStream,0)
};

  
// Local Variables:
//  mode: C++
// End:


